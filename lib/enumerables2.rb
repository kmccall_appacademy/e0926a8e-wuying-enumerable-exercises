require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.inject(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |string| string.include?(substring) }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  new_string = string.split.join
  obj = count_char(new_string)

  obj.keys.select { |key| key if obj[key] > 1 }
end

def count_char(new_string)
  char_count = {}

  (0..(new_string.length - 1)).each do |i|
    if char_count[new_string[i]]
      char_count[new_string[i]] += 1
    else
      char_count[new_string[i]] = 1
    end
  end

  char_count
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  longest_word = ""
  second_longest = ""

  string.split.map do |word|
    if word.length > longest_word.length
      second_longest = longest_word
      longest_word = word
    end
  end

  [longest_word, second_longest]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  arr = string.split("")
  missed_char = []

  ('a'..'z').map do |char|
    missed_char << char unless arr.include?(char)
  end

  missed_char
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |year| year if not_repeat_year?(year) }
end

def not_repeat_year?(year)
  year_string = year.to_s.split("")

  year_string.map.with_index do |digit, i|
    return false if year_string[(i + 1)..-1].include?(digit)
  end

  return true
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs_obj = all_songs_obj(songs)
  songs_obj.keys.select { |key| key if songs_obj[key] == 0 }
end

def all_songs_obj(songs)
  songs_obj = {}

  songs.uniq.each { |song_name| songs_obj[song_name] = 0 }

  songs.each.with_index do |song_name, i|
    if song_name == songs[i + 1]
      songs_obj[song_name] += 1
    end
  end

  songs_obj
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  return "" unless string.include?('c')
  closest_word = ""
  closest_c = -100

  remove_punctuation(string).each do |word|
    if word.include?('c') && c_distance(word) > closest_c
      closest_c = c_distance(word)
      closest_word = word
    end
  end

  closest_word
end

def c_distance(word)
  word.length.downto(0).each { |i| return i - word.length if word[i] == 'c' }
end

def remove_punctuation(string)
  removed_punc_words = string.delete("?!.,").split
end


# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  repeated_ranges = []
  start_index = 0
  end_index = 0

  arr.map.with_index do |num, i|
    if arr[i] == arr[i + 1] && arr[i] != arr[i - 1]
      start_index = i
    elsif arr[i] == arr[i - 1] && arr[i] != arr[i + 1]
      end_index = i
    else
      next
    end
    repeated_ranges << [start_index, end_index] unless end_index <= start_index
  end

  repeated_ranges
end
